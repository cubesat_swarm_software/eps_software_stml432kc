#ifndef FREERTOS_OBC_PEREPHERAL_H
#define FREERTOS_OBC_PEREPHERAL_H
#include "stm32l4xx_hal.h"
void sensors_init();
void i2c_Scan(I2C_HandleTypeDef *hi2c);
#endif //FREERTOS_OBC_PEREPHERAL_H
