/*
 * telemerty.h
 *
 *  Created on: 17 апр. 2021 г.
 *      Author: Ilia
 */

#ifndef COMMANDS_TELEMERTY_H_
#define COMMANDS_TELEMERTY_H_
#include "stm32l4xx_hal.h"
#include "board.h"
#include "time.h"

struct telemerty_reg {
	int16_t  temperature_param [T_SENS_EPS_LAST     - T_SENS_EPS_FIRST + 1];
	uint16_t current_param     [UI_SENSORS_EPS_LAST - UI_SENSORS_EPS_FIRST + 1];
	uint16_t voltage_param     [UI_SENSORS_EPS_LAST - UI_SENSORS_EPS_FIRST + 1];
	struct time_board time;
};

void telemerty_regylar();

struct telemerty_reg * telemerty_regular_get();

#endif /* COMMANDS_TELEMERTY_H_ */
