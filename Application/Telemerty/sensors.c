#include "stm32l4xx_hal.h"

#include "sensors.h"
#include "board.h"
#include "main.h"
#include "stdio.h"
#include "status.h"
#include "log_utils.h"
#include "errors.h"
#include "../../Application/Drivers/INA219.h"
#include "../../Application/Drivers/DS1631.h"
#include "../../Application/Drivers/PAC1934.h"

extern UART_HandleTypeDef *muart;

void sensors_init() {
	uint16_t error_id;
	struct status *dev_status = status_get();
    //HAL_GPIO_WritePin(EN1_GPIO_Port, EN1_Pin|EN2_Pin|EN4_Pin, GPIO_PIN_RESET); //Остальные включить
	error_id = INA219_SetCalibration(&I2C_EPS,INA219_CHRG1);
	dev_status->sens_err[SENS_EPS_INA219_CHRG1] = error_id==0? 0: ERR_INA219_CHRG1;

	error_id = INA219_SetCalibration(&I2C_EPS,INA219_CHRG2);
	dev_status->sens_err[SENS_EPS_INA219_CHRG2] = error_id==0? 0: ERR_INA219_CHRG2;

	error_id =INA219_SetCalibration(&I2C_EPS,INA219_VBAT3);
	dev_status->sens_err[SENS_EPS_INA219_VBAT3] = error_id==0? 0: ERR_INA219_VBAT3;

	error_id =INA219_SetCalibration(&I2C_EPS,INA219_BATP);
	dev_status->sens_err[SENS_EPS_INA219_BATP] = error_id==0? 0: ERR_INA219_BATP;

	error_id =INA219_SetCalibration(&I2C_EPS,INA219_VBAT2);
	dev_status->sens_err[SENS_EPS_INA219_VBAT2] = error_id==0? 0: SENS_EPS_INA219_VBAT2;

	error_id =INA219_SetCalibration(&I2C_EPS,INA219_BATC1);
	dev_status->sens_err[SENS_EPS_INA219_BATC1] = error_id==0? 0: SENS_EPS_INA219_BATC1;

	error_id =INA219_SetCalibration(&I2C_EPS,INA219_BATC2);
	dev_status->sens_err[SENS_EPS_INA219_BATC2] = error_id==0? 0: SENS_EPS_INA219_BATC2;

	error_id = DS1631_Init(DS1631_BAT1,&I2C_EPS)||
    		   DS1631_SetTH(DS1631_BAT1, DS1631_TH)||
			   DS1631_SetTL(DS1631_BAT1, DS1631_TL);
	dev_status->sens_err[SENS_EPC_DS1631_BAT1] = error_id==0? 0: SENS_EPC_DS1631_BAT1;

	error_id = DS1631_Init(DS1631_BAT2,&I2C_EPS)||
    		   DS1631_SetTH(DS1631_BAT2, DS1631_TH)||
			   DS1631_SetTL(DS1631_BAT2, DS1631_TL);
	dev_status->sens_err[SENS_EPC_DS1631_BAT2] = error_id==0? 0: SENS_EPC_DS1631_BAT2;

    error_id = PAC1934_Init(&I2C_EPS);
    dev_status->sens_err[SENS_EPS_PAC1934_CH1] = error_id==0? 0: SENS_EPS_PAC1934_CH1;
    dev_status->sens_err[SENS_EPS_PAC1934_CH2] = error_id==0? 0: SENS_EPS_PAC1934_CH1;
    dev_status->sens_err[SENS_EPS_PAC1934_CH3] = error_id==0? 0: SENS_EPS_PAC1934_CH1;
    dev_status->sens_err[SENS_EPS_PAC1934_CH4] = error_id==0? 0: SENS_EPS_PAC1934_CH1;
    PAC1934_RefreshV(&I2C_EPS);
    DEBUG_LED_ON;
    HAL_GPIO_WritePin(CAN2_SELECT_GPIO_Port, CAN2_SELECT_Pin, GPIO_PIN_SET);
    for (uint8_t i = 0; i < SENS_EPC_COUNT-1; i++) {
        if (dev_status->sens_err[i] != 0) {
            char str_send[30];
            sprintf(str_send, "ERROR WHILE INIT SENSOR %d\r\n", i);
            HAL_UART_Transmit(muart, (uint8_t *) str_send, strlen((char *) str_send), 100);
        }
    }
    i2c_Scan(&hi2c1);
}

void i2c_Scan(I2C_HandleTypeDef *hi2c){
    char uart2Data[24] = "Connected to UART\r\n";
    char str [255] = "";

    HAL_UART_Transmit(muart, (uint8_t *)&uart2Data,sizeof(uart2Data), 0xFFFF);

   	sprintf(str, "%s\r\n",str);
   	HAL_UART_Transmit(muart, (uint8_t *)str,strlen(str), 0xFFFF);
   	sprintf(str, "Scanning I2C bus:\r\n");
   	HAL_UART_Transmit(muart, (uint8_t *)str,strlen(str), 0xFFFF);
  	HAL_StatusTypeDef result;
   	uint8_t i= 0;
	for (i=1; i<128; i++)
   	{
   	  result = HAL_I2C_IsDeviceReady(hi2c, (uint16_t)(i<<1), 2, 2);
   	  if (result != HAL_OK) // HAL_ERROR or HAL_BUSY or HAL_TIMEOUT
   	  {
   		  sprintf(str, "."); // No ACK received at that address
   		  HAL_UART_Transmit(muart, (uint8_t *)str,strlen(str), 0xFFFF);
   	  }
   	  if (result == HAL_OK)
   	  {
   		  sprintf(str, "0x%X", i); // Received an ACK at that address
   		  HAL_UART_Transmit(muart, (uint8_t *)str,strlen(str), 0xFFFF);
   	  }
   	}
   	sprintf(str, "\r\n");
   	HAL_UART_Transmit(muart, (uint8_t *)str,strlen(str), 0xFFFF);

}
