/*
 * log_book.c

 *
 *  Created on: 27 ���. 2021 �.
 *      Author: Ilia
 */
#include "logbook.h"
#include "../Drivers/flash.h"

static uint32_t logbook_current_address;
static uint32_t logbook_record_num;

static uint32_t logbook_cur_address(void);
static uint32_t logbook_last_record_num(void);

uint16_t logbook_free(void){
	logbook_current_address = LOOGBOOK_ADDRESS_START;
	logbook_record_num = 0;
	return flash_erase(LOOGBOOK_ADDRESS_START, 1);
}

uint16_t logbook_write(struct Logbook_t* logbook) {
	if(logbook_current_address <LOOGBOOK_ADDRESS_END-sizeof(struct Logbook_t)){
		logbook->record_num = logbook_record_num+1;
	    time_get(&logbook->time);
	}
	else{
		return LOG_FULL;
	}
	uint16_t res = flash_save((void *)logbook, logbook_current_address, sizeof(struct Logbook_t));
	if(res==0){
		logbook_record_num +=1;
		logbook_current_address+=sizeof(struct Logbook_t);
	}
	return res;
}

uint16_t logbook_readlast(struct Logbook_t* logbook) {
	uint32_t read_adr = logbook_current_address;
	if(read_adr==LOOGBOOK_ADDRESS_START){
		return LOG_EMPTY;
	}
	read_adr-= sizeof(struct Logbook_t); //read last record
    uint32_t *dest_adr = (uint32_t *)logbook;
	for (uint16_t i=0; i < LOGBOOK_WORD_CNT; i=i+1) {
		*(dest_adr + i) = *( uint32_t*)(read_adr + 4*i);
	}
	return LOG_OK;
}

uint16_t logbook_read(struct Logbook_t* logbook, uint16_t record_num) {
	if(logbook_current_address==LOOGBOOK_ADDRESS_START){
		return LOG_EMPTY;
	}
	if((record_num>0)&&(record_num<=logbook_record_num)){
		uint32_t read_adr = LOOGBOOK_ADDRESS_START + (record_num-1)*sizeof(struct Logbook_t);
		uint32_t *dest_adr = (uint32_t *)logbook;
		for (uint16_t i=0; i < LOGBOOK_WORD_CNT; i=i+1) {
			*(dest_adr + i) = *( uint32_t*)(read_adr + 4*i);
		}
		return LOG_OK;
	}
	else{
		return LOG_ERROR;
	}
}

uint32_t logbook_get_size(void){
	return logbook_record_num;
}
void logbook_init(void){
	logbook_record_num = logbook_last_record_num();
	logbook_current_address = logbook_cur_address();
}

static uint32_t logbook_last_record_num(void) {
	uint32_t addr = logbook_cur_address();
	uint32_t record_num;
	if (addr==LOOGBOOK_ADDRESS_START){
		return 1;
	}
	else{
		record_num = *( __IO uint32_t*)(addr-sizeof(struct Logbook_t));
		return record_num;
	}
}

static uint32_t logbook_cur_address(void){
	uint32_t read_data, addr;
	for(addr = LOOGBOOK_ADDRESS_START;addr<=LOOGBOOK_ADDRESS_END; addr+=sizeof(struct Logbook_t)){
		read_data = *( __IO uint32_t*)(addr);
		if (read_data == 0xFFFFFFFF){
			break;
		}
	}
	return addr;
}
