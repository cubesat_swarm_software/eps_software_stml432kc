/*
 * array_utils.c
 *
 *  Created on: 4 апр. 2021 г.
 *      Author: Ilia
 */
#include "array_utils.h"

uint32_t array_pop32(uint8_t *data){
	uint32_t res  = data[0] | (data[1] << 8) | (data[2] << 16) | (data[3] << 24);
	return res;
}
uint16_t array_pop16(uint8_t *data){
	uint32_t res  = data[0] | (data[1] << 8);
	return res;
}
