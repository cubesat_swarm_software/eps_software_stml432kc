/*
 * time.c
 *
 *  Created on: 12 апр. 2021 г.
 *      Author: Ilia
 */


#include "time.h"
#include "rtc.h"

void time_get(struct time_board* time){
	RTC_TimeTypeDef currrentTime = {0};
	RTC_DateTypeDef currentDate = {0};
	HAL_RTC_GetTime(&hrtc, &currrentTime, RTC_FORMAT_BCD);
	HAL_RTC_GetDate(&hrtc, &currentDate, RTC_FORMAT_BCD);
	time->seconds = currrentTime.Seconds;
	time->minutes = currrentTime.Minutes;
	time->hours = currrentTime.Hours;
	time->days = currentDate.Date;
}

uint8_t time_set(RTC_TimeTypeDef *sTime,RTC_DateTypeDef *sDate){
	if (HAL_RTC_SetTime(&hrtc, sTime, RTC_FORMAT_BCD) != HAL_OK)
	{
		return 1;
	}

	if (HAL_RTC_SetDate(&hrtc, sDate, RTC_FORMAT_BCD) != HAL_OK)
	{
		return 2;
	}
	return 0;

}
