/*
 * defoult_settings.h
 *
 *  Created on: 29 ���. 2021 �.
 *      Author: Ilia
 */

#ifndef SETTINGS_DEFAULT_SETTINGS_H_
#define SETTINGS_DEFAULT_SETTINGS_H_
#define FLASH_APP_SIZE                      0x18000
#define FLASH_BOOTLOADER_ADDRESS            0x08000000
#define FLASH_APP_ADDRESS                   0x08008000
#define FLASH_CRC_APP                       (FLASH_APP_ADDRESS+FLASH_APP_SIZE-4)    //0x0801FFFC
#define FLASH_SECOND_APP_ADRESS             (FLASH_APP_ADDRESS+FLASH_APP_SIZE)      //0x08020000
#define FLASH_CRC_SECOND_APP                (FLASH_APP_ADDRESS+2*FLASH_APP_SIZE-4)  //0x08037FFC
#define FLASH_SETTINGS_ADDRESS              (FLASH_APP_ADDRESS+2*FLASH_APP_SIZE)    //0x08038000
#define FLASH_BOOTLOADER_FLAG               0x08038800
#define FLASH_LOGBOOK_ADDRESS               0x08039000      //233472 bytes from 256 kbyter for EPS and 1mbyte for OBC

#define DEV_SERIAL_LEN			            8
#define DEFAULT_DEVICE_SERIAL	            "EPS"
#define DEFAULT_FW_VERSION		            19
#define DEFAULT_CAN_STATION_ID              0x1
#define DEFAULT_CAN_DEVICE_ID	            0x4
#define DEFAULT_CAN_OBC_ISL_ID	            0x5
#define DEFAULT_CAN_OBC_ADCS	            0x6

#define DEFAULT_TEL_REG_PERIOD	            5000
#define DEFAULT_HEATBEAT_PEROOD             20000
#define DEFAULT_PLANNER_PEROOD              2000
#define MAX_HEATBEAT_FAIL                   5

#define DEFAULT_PCH_RESET_DELAY             500
#define DEFAULT_PCH_ENABLE                  1
#endif /* SETTINGS_DEFAULT_SETTINGS_H_ */
