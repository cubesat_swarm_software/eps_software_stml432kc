/*
 * status.c
 *
 *  Created on: 6 апр. 2021 г.
 *      Author: Ilia
 */
#include "status.h"
#include "settings.h"
static struct status dev_status;
struct status* status_get(){
	return &dev_status;
}
void status_init(void){
	struct settings* dev_settings = settings_get();
	dev_status.ch1 = 0;
	dev_status.ch1_enable = dev_settings->PCH1_enable;
	dev_status.ch2 = 0;
	dev_status.ch2_enable = dev_settings->PCH2_enable;
	dev_status.ch3 = 0;
	dev_status.ch3_enable = dev_settings->PCH3_enable;
	dev_status.ch4 = 0;
	dev_status.ch4_enable = dev_settings->PCH4_enable;
	dev_status.ch4 = 0;
	dev_status.ch4_enable = dev_settings->PCH4_enable;
	dev_status.ch_rfm = 0;
	dev_status.ch_rfm_enable = dev_settings->PCH_RFM_enable;
	dev_status.state = EPS_RFM;
	dev_status.tel_UHF_en = 0;
}
