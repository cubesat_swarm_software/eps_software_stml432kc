/*
 * status.h
 *
 *  Created on: 6 апр. 2021 г.
 *      Author: Ilia
 */

#ifndef SETTINGS_STATUS_H_
#define SETTINGS_STATUS_H_
#include "board.h"

#pragma push(pack)
#pragma pack(1)
struct status {
	uint8_t state;
    uint8_t ch1:1;
    uint8_t ch1_reset_n;
    uint8_t ch1_enable:1;
    uint8_t ch2:1;
    uint8_t ch2_reset_n;
    uint8_t ch2_enable:1;
    uint8_t ch3:1;
    uint8_t ch3_reset_n;
    uint8_t ch3_enable:1;
    uint8_t ch4:1;
    uint8_t ch4_reset_n;
    uint8_t ch4_enable:1;
    uint8_t ch_rfm:1;
    uint8_t ch_rfm_reset_n;
    uint8_t ch_rfm_enable:1;
    uint8_t tel_UHF_en:1;
    uint8_t sens_err[SENS_EPC_COUNT];
};
#pragma pop(pack)

typedef enum {EPS_RFM, OBC_UHF, OBC_ISL} status_state;
struct status* status_get();
void status_init(void);
#endif /* SETTINGS_STATUS_H_ */
