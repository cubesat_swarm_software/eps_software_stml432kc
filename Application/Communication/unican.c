

#include <default_settings.h>
#include "unican.h"
#include "string.h"
#include "ring_buffer.h"
//#include "crc.h"  //remove ring_buffer and add crc instead in future
#include "log_utils.h"
#include "settings.h"
#include "status.h"
#include "errors.h"
const static char *tag = "Unican";

static struct {
	struct unican_handler *first;
	CAN_HandleTypeDef     *hcan;
	QueueHandle_t          out_queue;
    uint8_t                in_respond_buffer_mem[UNICAN_RESPOND_BUFFER_SIZE];
    StaticMessageBuffer_t  in_respond_buffer;
	SemaphoreHandle_t      start_transmition_smphr;
	StaticSemaphore_t      start_transmition_smphr_buffer;
	SemaphoreHandle_t      queue_smphr;
	StaticSemaphore_t      queue_smphr_buffer;
} unican_state;

static void unican_rx_fifo0_cb(CAN_HandleTypeDef *hcan);

static void unican_rx_fifo1_cb(CAN_HandleTypeDef *hcan);

static void unican_rx_cb(CAN_HandleTypeDef *hcan, uint32_t rx_fifo);

static void unican_tx_mbox0_cb(CAN_HandleTypeDef *hcan);

static void unican_tx_mbox1_cb(CAN_HandleTypeDef *hcan);

static void unican_tx_mbox2_cb(CAN_HandleTypeDef *hcan);

static void unican_tx_cb(CAN_HandleTypeDef *hcan, uint32_t mbox);

static struct unican_handler **unican_tail_handler();

struct unican_handler *unican_find_by_address(uint8_t address);

static bool unican_try_transmit(bool from_isr);

static void unican_add_crc16(const uint8_t *data, uint8_t *data_crc,uint8_t len);

struct unican_respond_result unican_get_responce(uint8_t address,uint16_t msg_id);

void unican_init(CAN_HandleTypeDef *hcan) {
    struct settings *dev_settings = settings_get();
	unican_state.first = NULL;
	unican_state.hcan = hcan;
	unican_state.start_transmition_smphr = xSemaphoreCreateMutexStatic(&unican_state.start_transmition_smphr_buffer);
	xSemaphoreGive(unican_state.start_transmition_smphr);
	unican_state.queue_smphr = xSemaphoreCreateMutexStatic(&unican_state.queue_smphr_buffer);
	xSemaphoreGive(unican_state.queue_smphr);
	unican_state.out_queue = xQueueCreate( UNNICAN_OUT_QUEUE_SIZE, sizeof(struct can_msg_tx) );
	xMessageBufferCreateStatic(sizeof(unican_state.in_respond_buffer_mem), unican_state.in_respond_buffer_mem,
							   &unican_state.in_respond_buffer);
	HAL_CAN_RegisterCallback(hcan, HAL_CAN_RX_FIFO0_MSG_PENDING_CB_ID, unican_rx_fifo0_cb);
	HAL_CAN_RegisterCallback(hcan, HAL_CAN_RX_FIFO1_MSG_PENDING_CB_ID, unican_rx_fifo1_cb);
	HAL_CAN_RegisterCallback(hcan, HAL_CAN_TX_MAILBOX0_COMPLETE_CB_ID, unican_tx_mbox0_cb);
	HAL_CAN_RegisterCallback(hcan, HAL_CAN_TX_MAILBOX1_COMPLETE_CB_ID, unican_tx_mbox1_cb);
	HAL_CAN_RegisterCallback(hcan, HAL_CAN_TX_MAILBOX2_COMPLETE_CB_ID, unican_tx_mbox2_cb);
	HAL_CAN_ActivateNotification(hcan, CAN_IT_RX_FIFO0_MSG_PENDING | CAN_IT_RX_FIFO1_MSG_PENDING | CAN_IT_TX_MAILBOX_EMPTY);

	CAN_FilterTypeDef filter;
	filter.FilterBank = 0;
	filter.FilterMode = CAN_FILTERMODE_IDMASK;
	filter.FilterScale = CAN_FILTERSCALE_32BIT;
	filter.FilterIdHigh = dev_settings-> can_device_id<<5;
	filter.FilterIdLow = 0x0000;
	filter.FilterMaskIdHigh = 0x0000;
	filter.FilterMaskIdLow = 0x0000;
	filter.FilterFIFOAssignment = CAN_RX_FIFO0 | CAN_RX_FIFO1;
	filter.FilterActivation = ENABLE;
	filter.SlaveStartFilterBank = 14;
	HAL_CAN_ConfigFilter(hcan, &filter);

	HAL_CAN_Start(hcan);
}

struct unican_handler **unican_tail_handler() {
	struct unican_handler **p = &unican_state.first;

	while(*p != NULL) {
		p = &((*p)->next);
	}

	return p;
}

struct unican_handler *unican_find_by_address(uint8_t address) {
	struct unican_handler *h;
	uint8_t id = UNICAN_HANDLERID_COMMAND;
	struct unican_handler **p = &unican_state.first;
	struct unican_handler *pp;
	while(*p != NULL) {
		pp = *p;
		if (pp->id == id) {
			return *p;
		}
		p = &((*p)->next);
	}

	return NULL;
}

void unican_add_handler(uint8_t id, struct unican_handler *handler) {
	handler->id = id;
	handler->next = NULL;
	xMessageBufferCreateStatic(sizeof(handler->in_message_buffer_mem), handler->in_message_buffer_mem,
							   &handler->in_message_buffer);

	struct unican_handler **last = unican_tail_handler();
	*last = handler;

}

static uint8_t length;
static RING_buffer_t ring_Rx;
static RING_buffer_t ring_Tx;
static uint8_t data_rx_buf_crc[UNICAN_OUT_BUFFER_SIZE]; //storage for calculating crc16

struct unican_respond_result unican_get_responce(uint8_t address,uint16_t msg_id) {
	struct unican_respond msg_resp;
	struct unican_respond_result res;
	size_t  n_bytes;
	for(int i=0;i<UNICAN_MAX_TIMEOUT_COUNT;i++){
		n_bytes = xMessageBufferReceive(&unican_state.in_respond_buffer, &msg_resp, sizeof(struct unican_respond), pdMS_TO_TICKS(UNICAN_TIMEOUT_CMD));
	    if (n_bytes) break;
	}
	if (n_bytes==0){
		res.type = TIMEOUT;
	}
	else{
		if((msg_resp.address==address)&&(msg_resp.msg_id = msg_id)){
			res.type = (msg_resp.type==ACK)? OK: ERR;
		}
		else{
			res.type = ANKNOWN_RESP;
		}
	}
	res.value = msg_resp.value;
	return res;
}

bool unican_receive(struct unican_handler *h, uint32_t timeout) {
	struct can_msg_rx msg_can;
	size_t size = xMessageBufferReceive(&h->in_message_buffer, &msg_can, sizeof(struct can_msg_rx), pdMS_TO_TICKS(timeout));
	struct unican_canid *id = &msg_can.header.StdId;
	uint8_t dls = (uint8_t)msg_can.header.DLC;
	uint16_t start_id =  ((uint16_t)(msg_can.data[1] << 8) | msg_can.data[0]);
	h->packet.sender = id->sender;
	if(size){
	if ((start_id!=UNICAN_LONG_START_ID)&&(id->data==0)){          //SHORT MSG
		LOGI(tag,"GOT SHORT");
		RING_Init(&ring_Rx,h->packet.data, UNICAN_MAX_PAYLOAD_SIZE);
		RING_PutBuffr(&ring_Rx,msg_can.data+2,dls-2);
		h->packet.msg_id =  start_id;
		h->packet.data_len = dls-2;
		return true;
	}

	if ((start_id==UNICAN_LONG_START_ID)&&(id->data==0)){               //// START OF LONG MSG
		LOGI(tag,"GOT LONG");
		RING_Init(&ring_Rx,h->packet.data, UNICAN_MAX_PAYLOAD_SIZE);
		length = ((uint16_t)(msg_can.data[5] << 8) | msg_can.data[4]);
		if(length>=UNICAN_MAX_PAYLOAD_SIZE){
			length = 0;
			unican_nack(id->sender,h->packet.msg_id,ERR_UNICAN_TOO_LONG);
		}
		h->packet.msg_id =  ((uint16_t)(msg_can.data[3] << 8) | msg_can.data[2]);
		h->packet.data_len =  length-2;
		return false;
	}
	else if(id->data){ //// DATA OF LONG MSG
		length-=dls;
		if(length){
			RING_PutBuffr(&ring_Rx,msg_can.data,dls);
		}
		else{
			RING_PutBuffr(&ring_Rx,msg_can.data,dls-2);
			uint16_t crc16_houston =(((uint16_t)msg_can.data[dls-1] << 8) | msg_can.data[dls-2]);
			uint16_t crc16 = crc16_X_Modern(&ring_Rx,h->packet.data_len,0);
			if(crc16_houston==crc16){
				return true;
			}
			else{
				unican_nack(id->sender,h->packet.msg_id, ERR_UNICAN_CRC);
				LOGI(tag,"WRONG CRC");
			}
		}
	}
	}
	return false;
}

void unican_add_crc16(const uint8_t *data, uint8_t *data_crc,uint8_t len){
	RING_Init(&ring_Tx,data_crc, UNICAN_OUT_BUFFER_SIZE);
	RING_PutBuffr(&ring_Tx,data,len);
	uint16_t crc16 = crc16_X_Modern(&ring_Tx,len,0);
	RING_Put16(&ring_Tx,crc16);
}


bool unican_send_msg( struct unican_message * unican_msg){
	struct can_msg_tx msg;
	msg.header.TransmitGlobalTime = DISABLE;
	msg.header.RTR = CAN_RTR_DATA;
	msg.header.IDE = CAN_ID_STD;
	struct unican_canid *canid = &msg.header.StdId;
	union unican_can_msg_data *can_data = &msg.data;
    struct settings *dev;
    dev = settings_get();
	canid->sender = dev->can_device_id;
	canid->receiver = unican_msg->unican_address_to;
	canid->data = 0;

	if (unican_msg->unican_length <= UNICAN_SHORT_PAYLOAD_SIZE) {
		msg.header.DLC = UNICAN_DLC(unican_msg->unican_length);
		can_data->short_msg.msg_id = unican_msg->unican_msg_id;
		if(unican_msg->unican_length){
			memcpy(&can_data->short_msg.payload, unican_msg->data, unican_msg->unican_length);
		}
		xQueueSendToBack(unican_state.out_queue,  ( void * ) &msg, portMAX_DELAY);
	} else {
		unican_add_crc16(unican_msg->data,data_rx_buf_crc,unican_msg->unican_length);
		can_data->start_msg.start_id = UNICAN_LONG_START_ID;
		can_data->start_msg.msg_id = unican_msg->unican_msg_id;
		can_data->start_msg.len = UNICAN_DLC(unican_msg->unican_length);
		msg.header.DLC = UNICAN_START_SIZE;
		xQueueSendToBack(unican_state.out_queue,  ( void * ) &msg, portMAX_DELAY);

		size_t bytes_left = UNICAN_DLC(unican_msg->unican_length);
		canid->data = 1;
		uint8_t *p = data_rx_buf_crc;
		while(bytes_left > 0) {
			size_t bytes_to_sent = bytes_left > UNICAN_LONG_PAYLOAD_SIZE ?
				UNICAN_LONG_PAYLOAD_SIZE :
				bytes_left;

			msg.header.DLC = bytes_to_sent;
			memcpy(can_data->data_msg.payload, p, bytes_to_sent);
			xQueueSendToBack(unican_state.out_queue,  ( void * ) &msg, portMAX_DELAY);
			bytes_left -= bytes_to_sent;
			p += bytes_to_sent;
		}
	}
	return unican_try_transmit(false);
}

struct unican_respond_result unican_send_cmd( struct unican_message * unican_msg){
	struct unican_respond_result res;
	if(unican_send_msg(unican_msg)){
		res = unican_get_responce(unican_msg->unican_address_to,unican_msg->unican_msg_id);
	}
	else{
		res.type = TRANSMIT_ERR;
	}
	return res;
}

bool unican_try_transmit(bool from_isr) {

	bool is_smf_taken;
	bool ret = false;
	if (from_isr) {
		if ( xQueueIsQueueEmptyFromISR(unican_state.out_queue)) {
			return false;
		}
		is_smf_taken = xSemaphoreTakeFromISR(unican_state.start_transmition_smphr, NULL);
	} else {
		is_smf_taken = xSemaphoreTake(unican_state.start_transmition_smphr,( TickType_t )1000);
	}
	if(!is_smf_taken){
		return false;
	}
	bool any_mbox_empty =
			__HAL_CAN_GET_FLAG(unican_state.hcan, CAN_FLAG_TME0) ||
			__HAL_CAN_GET_FLAG(unican_state.hcan, CAN_FLAG_TME1) ||
			__HAL_CAN_GET_FLAG(unican_state.hcan, CAN_FLAG_TME2);

	if (any_mbox_empty) {
		struct can_msg_tx msg;
		if (from_isr) {
			xQueueReceiveFromISR(unican_state.out_queue, &msg, NULL);
		} else {
			xQueueReceive(unican_state.out_queue, &msg,  ( TickType_t ) 1000);
		}
		uint32_t mbox[10];
		if(HAL_CAN_AddTxMessage(unican_state.hcan, &msg.header, msg.data, mbox) != HAL_OK) {
			ret = false;
		}

	}

	if (from_isr) {
		xSemaphoreGiveFromISR(unican_state.start_transmition_smphr, NULL);
	} else {
		xSemaphoreGive(unican_state.start_transmition_smphr);
	}
	return ret;
}

void unican_rx_fifo0_cb(CAN_HandleTypeDef *hcan) {
	unican_rx_cb(hcan, 0);
}

void unican_rx_fifo1_cb(CAN_HandleTypeDef *hcan) {
	unican_rx_cb(hcan, 1);
}

void unican_rx_cb(CAN_HandleTypeDef *hcan, uint32_t rx_fifo) {
	struct can_msg_rx msg;
	HAL_CAN_GetRxMessage(hcan, rx_fifo, &msg.header, msg.data);
	struct unican_canid *id = &msg.header.StdId;
	if(msg.header.DLC<2){
		unican_nack(id->sender,0,ERR_UNICAN_TOO_SHORT);
	}
	else{
	    struct settings *dev;
	    dev = settings_get();
		if (id->receiver == dev->can_device_id){
			uint16_t msg_id =  ((uint16_t)(msg.data[1] << 8) | msg.data[0]);
			if(((msg_id==UNICAN_ACK)||(msg_id==UNICAN_NACK))&&(id->data==0)){
				struct unican_respond resp;
				resp.type = (msg_id==UNICAN_ACK)? ACK: NACK;
				resp.address = id->sender;
				resp.msg_id = ((uint16_t)(msg.data[3] << 8) | msg.data[2]);;
				resp.value = msg_id =UNICAN_NACK? ((uint16_t)(msg.data[5] << 8) | msg.data[4]): 0;
				xMessageBufferSendFromISR(&unican_state.in_respond_buffer, &resp, sizeof(resp), NULL);
			}
			else{
				struct unican_handler *h = unican_find_by_address(id->sender);
				if(h!=NULL){
					xMessageBufferSendFromISR(&h->in_message_buffer, &msg, sizeof(msg), NULL);
				}
			}
		}
	}
}


void unican_tx_mbox0_cb(CAN_HandleTypeDef *hcan) {
	unican_tx_cb(hcan, CAN_TX_MAILBOX0);
}

void unican_tx_mbox1_cb(CAN_HandleTypeDef *hcan) {
	unican_tx_cb(hcan, CAN_TX_MAILBOX1);
}

void unican_tx_mbox2_cb(CAN_HandleTypeDef *hcan) {
	unican_tx_cb(hcan, CAN_TX_MAILBOX2);
}

void unican_tx_cb(CAN_HandleTypeDef *hcan, uint32_t mbox) {
	unican_try_transmit(true);
}

bool unican_send_u16(uint8_t dest_addr, uint16_t value, uint16_t msg_id){
	uint8_t data[2];
	data[1] = (uint8_t)((value & 0xFF00) >> 8);
	data[0] = (uint8_t)(value & 0x00FF);
	struct unican_message msg;
	msg.data = data;
	msg.unican_address_to   = dest_addr;
	msg.unican_length = 2;
	msg.unican_msg_id = msg_id;
	return unican_send_msg(&msg);
}

bool unican_ack(uint8_t dest_addr, uint16_t msg_id){
	return unican_send_u16(dest_addr,msg_id,UNICAN_ACK);
}

bool unican_nack(uint8_t dest_addr, uint16_t msg_id,uint16_t value){
	uint8_t data[4];
	data[0] = (uint8_t)(msg_id & 0x00FF);
	data[1] = (uint8_t)((msg_id & 0xFF00) >> 8);
	data[2] = (uint8_t)(value & 0x00FF);
	data[3] = (uint8_t)((value & 0xFF00) >> 8);
	struct unican_message msg;
	msg.data = data;
	msg.unican_address_to   = dest_addr;
	msg.unican_length = 4;
	msg.unican_msg_id = UNICAN_NACK;
	return unican_send_msg(&msg);
}

bool unican_hearbeat(uint8_t dest_addr){
	struct unican_message msg;
	msg.data = NULL;
	msg.unican_address_to   = dest_addr;
	msg.unican_length = 0;
	msg.unican_msg_id = UNICAN_HEATBEAT;
	return unican_send_msg(&msg);
}
