#include "main.h"
#include "i2c.h"
#include "stm32l4xx_hal.h"
#include "stdlib.h"

/**************************************************************************/

///*EPS_EQM
//sensors I2C address
#define INA219_NMAX			16
#define INA219_START		0b1000000
//OBC
#define INA219_BB_VCC 		0b1000000
#define INA219_COIL_VCC 	0b1000001
#define INA219_OBC_3V3		0b1000100
//EPS
#define INA219_BATC1		0b1000000
#define INA219_CHRG1		0b1000001
#define INA219_BATC2		0b1000010
#define INA219_VBAT2		0b1000011
#define INA219_VBAT3		0b1000100
#define INA219_BATP			0b1000101

#define INA219_CHRG2		0b1001100

#define INA219_STOP			0b1001100
//*/
/*EPS Gosha
//sensors I2C address
#define INA219_NMAX			16
#define INA219_START		0b1000000
//OBC
#define INA219_BB_VCC 		0b1000000
#define INA219_COIL_VCC 	0b1000001
#define INA219_OBC_3V3		0b1000100
//EPS
#define INA219_BATC1		0b1000000
#define INA219_CHRG1		0b1000001
#define INA219_BATC2		0b1000010
#define INA219_VBAT2		0b1000011
#define INA219_VBAT3		0b1000100
#define INA219_BATP			0b1000101
#define INA219_PCH1			0b1000110
#define INA219_PCH2			0b1000111
#define INA219_PCH3			0b1001000
#define INA219_PCH4			0b1001001
#define INA219_CHRG2		0b1001010
#define INA219_BATC			0b1001011
#define INA219_TRMST1		0b1001110
#define INA219_TRMST2		0b1001111

#define INA219_STOP			0b1001111
//*/


/*!
	@brief  read
*/
/**************************************************************************/
//	const uint8_t READ              =              (0x01);
#define READ 0x01
/*=========================================================================
	CONFIG REGISTER (R/W)
	-----------------------------------------------------------------------*/
/**************************************************************************/
/*!
	@brief  config register address
*/
/**************************************************************************/
//	const uint8_t REG_CONFIG         =             (0x00);
#define REG_CONFIG 0x00
	/*---------------------------------------------------------------------*/
/**************************************************************************/
/*!
	@brief  reset bit
*/
/**************************************************************************/
//	const uint16_t CONFIG_RESET       =             (0x8000);  // Reset Bit
#define CONFIG_RESET 0x8000
/**************************************************************************/
/*!
	@brief  mask for bus voltage range
*/
/**************************************************************************/
//	const uint16_t CONFIG_BVOLTAGERANGE_MASK   =    (0x2000);  // Bus Voltage Range Mask
#define CONFIG_BVOLTAGERANGE_MASK 0x2000
/**************************************************************************/
/*!
	@brief  bus voltage range values
*/
/**************************************************************************/
#define CONFIG_BVOLTAGERANGE_16V        0x0000  // 0-16V Range
#define CONFIG_BVOLTAGERANGE_32V        0x2000  // 0-32V Range

/**************************************************************************/
/*!
	@brief  mask for gain bits
*/
/**************************************************************************/
//	const uint16_t CONFIG_GAIN_MASK       =         (0x1800);  // Gain Mask
#define CONFIG_GAIN_MASK 0x1800
/**************************************************************************/
/*!
	@brief  values for gain bits
*/
/**************************************************************************/
#define CONFIG_GAIN_1_40MV               0x0000  // Gain 1, 40mV Range
#define CONFIG_GAIN_2_80MV               0x0800  // Gain 2, 80mV Range
#define CONFIG_GAIN_4_160MV              0x1000  // Gain 4, 160mV Range
#define CONFIG_GAIN_8_320MV              0x1800  // Gain 8, 320mV Range

  /**************************************************************************/
/*!
	@brief  mask for bus ADC resolution bits
*/
/**************************************************************************/
//	const uint16_t CONFIG_BADCRES_MASK      =       (0x0780);  // Bus ADC Resolution Mask
#define CONFIG_BADCRES_MASK 0x0780
/**************************************************************************/
/*!
	@brief  values for bus ADC resolution
*/
/**************************************************************************/
#define CONFIG_BADCRES_9BIT              0x0000  // 9-bit bus res = 0..511
#define CONFIG_BADCRES_10BIT             0x0080  // 10-bit bus res = 0..1023
#define CONFIG_BADCRES_11BIT             0x0100  // 11-bit bus res = 0..2047
#define CONFIG_BADCRES_12BIT             0x0180  // 12-bit bus res = 0..4097

/**************************************************************************/
/*!
	@brief  mask for shunt ADC resolution bits
*/
/**************************************************************************/
//	const uint8_t CONFIG_SADCRES_MASK      =       (0x0078);  // Shunt ADC Resolution and Averaging Mask
#define CONFIG_SADCRES_MASK 0x0078
/**************************************************************************/
/*!
	@brief  values for shunt ADC resolution
*/
/**************************************************************************/
#define CONFIG_SADCRES_9BIT_1S_84US      0x0000  // 1 x 9-bit shunt sample
#define CONFIG_SADCRES_10BIT_1S_148US    0x0008  // 1 x 10-bit shunt sample
#define CONFIG_SADCRES_11BIT_1S_276US    0x0010  // 1 x 11-bit shunt sample
#define CONFIG_SADCRES_12BIT_1S_532US    0x0018  // 1 x 12-bit shunt sample
#define CONFIG_SADCRES_12BIT_2S_1060US   0x0048	 // 2 x 12-bit shunt samples averaged together
#define CONFIG_SADCRES_12BIT_4S_2130US   0x0050  // 4 x 12-bit shunt samples averaged together
#define CONFIG_SADCRES_12BIT_8S_4260US   0x0058  // 8 x 12-bit shunt samples averaged together
#define CONFIG_SADCRES_12BIT_16S_8510US  0x0060  // 16 x 12-bit shunt samples averaged together
#define CONFIG_SADCRES_12BIT_32S_17MS    0x0068  // 32 x 12-bit shunt samples averaged together
#define CONFIG_SADCRES_12BIT_64S_34MS    0x0070  // 64 x 12-bit shunt samples averaged together
#define CONFIG_SADCRES_12BIT_128S_69MS   0x0078  // 128 x 12-bit shunt samples averaged together

/**************************************************************************/
/*!
	@brief  mask for operating mode bits
*/
/**************************************************************************/
//	const uint8_t CONFIG_MODE_MASK       =         (0x0007);  // Operating Mode Mask
#define CONFIG_MODE_MASK 0x0007
/**************************************************************************/
/*!
	@brief  values for operating mode
*/
/**************************************************************************/
#define CONFIG_MODE_POWERDOWN            0x0000
#define CONFIG_MODE_SVOLT_TRIGGERED     0x0001
#define CONFIG_MODE_BVOLT_TRIGGERED      0x0002
#define CONFIG_MODE_SANDBVOLT_TRIGGERED  0x0003
#define CONFIG_MODE_ADCOFF               0x0004
#define CONFIG_MODE_SVOLT_CONTINUOUS    0x0005
#define CONFIG_MODE_BVOLT_CONTINUOUS     0x0006
#define CONFIG_MODE_SANDBVOLT_CONTINUOUS  0x0007

//	const uint8_t REG_SHUNTVOLTAGE    =            (0x01);
#define REG_SHUNTVOLTAGE 0x01
//	const uint8_t REG_BUSVOLTAGE       =           (0x02);
#define REG_BUSVOLTAGE 0x02
//	const uint8_t REG_POWER             =          (0x03);
#define REG_POWER 0x03
//	const uint8_t REG_CURRENT           =         (0x04);
#define REG_CURRENT 0x04
//	const uint8_t REG_CALIBRATION        =         (0x05);
#define REG_CALIBRATION 0x05

uint8_t INA219_SetCalibration(I2C_HandleTypeDef *hi2c, uint8_t address);
uint8_t wireWriteRegister (I2C_HandleTypeDef *hi2c, uint8_t address,uint8_t reg, uint16_t value);
uint8_t wireReadRegister(I2C_HandleTypeDef *hi2c, uint8_t address, uint8_t reg, uint16_t *value);
float getShuntVoltage_raw(I2C_HandleTypeDef *hi2c, uint8_t address);
uint8_t getCurrent_raw(I2C_HandleTypeDef *hi2c, uint8_t address,int16_t * rawValue);
uint8_t getCurrentAmps(I2C_HandleTypeDef *hi2c, uint8_t address,float * amps);
uint8_t INA219_GetUI(I2C_HandleTypeDef *hi2c, uint8_t address,uint16_t * volts, uint16_t * amps);
