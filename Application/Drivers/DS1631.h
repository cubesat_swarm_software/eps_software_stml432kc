/*
 * DS1631.h
 *
 *  Created on: 12 окт. 2020 г.
 *      Author: Данил
 */

#ifndef INC_DS1631_H_
#define INC_DS1631_H_
#endif /* INC_DS1631_H_ */


#include "main.h"
#include "i2c.h"
#include "stm32l4xx_hal.h"

#define DS1631_BAT1 		0b1001110
#define DS1631_BAT2 		0b1001111

#define DS1631_TL			(30 << 8)
#define DS1631_TH			(34 << 8)

#define DS1631_START_CONV_T		0x51
#define DS1631_STOP_CONV_T		0x22
#define DS1631_READ_TEMP		0xAA
#define DS1631_ACCESS_TH		0xA1
#define DS1631_ACCESS_TL		0xA2
#define DS1631_ACCESS_CONF		0xAC
#define DS1631_SOFT_POR			0x54

#define DS1631_CONFIG_REG_1SH	0
#define DS1631_CONFIG_REG_POL	1
#define DS1631_CONFIG_REG_R0	2
#define DS1631_CONFIG_REG_R1	3
#define DS1631_CONFIG_REG_NVB	4
#define DS1631_CONFIG_REG_TLF	5
#define DS1631_CONFIG_REG_THF	6
#define DS1631_CONFIG_REG_DONE	7

uint8_t DS1631_WriteRegister (uint8_t address, uint8_t reg, uint8_t value);
uint8_t DS1631_ReadByte(uint8_t address, uint8_t reg, uint8_t *value);
uint8_t DS1631_ReadRegister(uint8_t address, uint8_t reg, uint16_t *value);
uint8_t DS1631_Init(uint8_t address, I2C_HandleTypeDef  * hi2c);
uint8_t DS1631_GetTemperature(uint8_t address, int16_t *value);
uint8_t DS1631_SetTHyst(uint8_t address, int16_t value);
uint8_t DS1631_SetTOs(uint8_t address, int16_t value);
