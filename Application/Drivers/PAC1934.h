#include "stm32l4xx_hal.h"
#include "stdlib.h"
#include "main.h"

#define PAC1934_addr			0b0010000

//REGISTER SET IN HEXADECIMAL ORDER
#define PAC1934_REFRESH			0x00
#define PAC1934_CTRL			0x01
#define PAC1934_ACC_COUNT		0x02
#define PAC1934_VPOWER1_ACC		0x03
#define PAC1934_VPOWER2_ACC		0x04
#define PAC1934_VPOWER3_ACC		0x05
#define PAC1934_VPOWER4_ACC		0x06
#define PAC1934_VBUS1			0x07
#define PAC1934_VBUS2			0x08
#define PAC1934_VBUS3			0x09
#define PAC1934_VBUS4			0x0A
#define PAC1934_VSENSE1			0x0B
#define PAC1934_VSENSE2			0x0C
#define PAC1934_VSENSE3			0x0D
#define PAC1934_VSENSE4			0x0E
#define PAC1934_VBUS1_AVG		0x0F
#define PAC1934_VBUS2_AVG		0x10
#define PAC1934_VBUS3_AVG		0x11
#define PAC1934_VBUS4_AVG		0x12
#define PAC1934_VSENSE1_AVG		0x13
#define PAC1934_VSENSE2_AVG		0x14
#define PAC1934_VSENSE3_AVG		0x15
#define PAC1934_VSENSE4_AVG		0x16
#define PAC1934_VPOWER1			0x17
#define PAC1934_VPOWER2			0x18
#define PAC1934_VPOWER3			0x19
#define PAC1934_VPOWER4			0x1A

#define PAC1934_CHANNEL_DIS		0x1C
#define PAC1934_NEG_PWR			0x1D
#define PAC1934_REFRESH_G		0x1E
#define PAC1934_REFRESH_V		0x1F

#define PAC1934_SLOW			0x20
#define PAC1934_CTRL_ACT		0x21
#define PAC1934_CHANNEL_DIS_ACT	0x22
#define PAC1934_NEG_PWR_ACT		0x23
#define PAC1934_CTRL_LAT		0x24
#define PAC1934_CHANNEL_DIS_LAT	0x25
#define PAC1934_NEG_PWR_LAT		0x26

#define PAC1934_PROD_ID_REG		0xFD
#define PAC1934_MANUF_ID_REG	0xFE
#define PAC1934_REV_ID_REG		0xFF

//CTRL REGISTER (ADDRESS 01H)
#define PAC1934_CTRL_OVF		0
#define PAC1934_CTRL_OVF_ALERT	1
#define PAC1934_CTRL_ALERT_CC	2
#define PAC1934_CTRL_ALERT_PIN	3
#define PAC1934_CTRL_SING		4
#define PAC1934_CTRL_SLEEP		5
#define PAC1934_CTRL_SR0		6
#define PAC1934_CTRL_SR1		7

//CTRL REGISTER Sample Rate
#define PAC1934_CTRL_SR_1024	0b00000000
#define PAC1934_CTRL_SR_256		0b01000000
#define PAC1934_CTRL_SR_64		0b10000000
#define PAC1934_CTRL_SR_8		0b11000000

//CHANNEL_DIS AND SMBUS (ADDRESS 1CH)
#define PAC1934_CHANNEL_DIS_NO_SKIP		1
#define PAC1934_CHANNEL_DIS_BYTE_COUNT	2
#define PAC1934_CHANNEL_DIS_TIMEOUT		3
#define PAC1934_CHANNEL_DIS_CH4_OFF		4
#define PAC1934_CHANNEL_DIS_CH3_OFF		5
#define PAC1934_CHANNEL_DIS_CH2_OFF		6
#define PAC1934_CHANNEL_DIS_CH1_OFF		7

//NEG_PWR (ADDRESS 1DH)
/*
	BIDI 0 = Channel  VSENSE ADC converts 0 to +100 mV range with 16-bit straight binary output
	BIDI 1 = Channel  VSENSE ADC converts -100 mV to +100 mV range with 16-bit two’s complement output
	BIDV 0 = Channel  VBUS ADC converts 0 to +32V range with 16-bit straight binary output
	BIDV 1 = Channel  VBUS ADC converts -32V to +32 range with 16-bit two’s complement output
*/
#define PAC1934_NEG_PWR_CH1_BIDI		7
#define PAC1934_NEG_PWR_CH2_BIDI		6
#define PAC1934_NEG_PWR_CH3_BIDI		5
#define PAC1934_NEG_PWR_CH4_BIDI		4
#define PAC1934_NEG_PWR_CH1_BIDV		3
#define PAC1934_NEG_PWR_CH2_BIDV		2
#define PAC1934_NEG_PWR_CH3_BIDV		1
#define PAC1934_NEG_PWR_CH4_BIDV		0

//SLOW (ADDRESS 20H)
#define PAC1934_SLOW_POR				0
#define PAC1934_SLOW_R_V_FALL			1
#define PAC1934_SLOW_R_FALL				2
#define PAC1934_SLOW_R_V_RISE			3
#define PAC1934_SLOW_R_RISE				4
#define PAC1934_SLOW_SLOW_HL			5
#define PAC1934_SLOW_SLOW_LH			6
#define PAC1934_SLOW_SLOW				7

//CTRL_ACT REGISTER (ADDRESS 21H)
#define PAC1934_CTRL_ACT_OVF			0
#define PAC1934_CTRL_ACT_OVF_ALERT		1
#define PAC1934_CTRL_ACT_ALERT_CC		2
#define PAC1934_CTRL_ACT_ALERT_PIN		3
#define PAC1934_CTRL_ACT_SING			4
#define PAC1934_CTRL_ACT_SLEEP			5
#define PAC1934_CTRL_ACT_SR0			6
#define PAC1934_CTRL_ACT_SR1			7

//OBC
#define PAC1934_COIL_X					0
#define PAC1934_COIL_Y					1
#define PAC1934_COIL_Z					2

//EPS
#define PAC1934_PCH3					0
#define PAC1934_PCH1					1
#define PAC1934_PCH2					2
#define PAC1934_PCH4					3

uint8_t PAC1934_WriteRegister (I2C_HandleTypeDef *hi2c, uint8_t address,uint8_t reg, uint8_t value);
uint8_t PAC1934_ReadRegister(I2C_HandleTypeDef *hi2c, uint8_t address, uint8_t reg, uint16_t *value);
uint8_t PAC1934_ReadByte(I2C_HandleTypeDef *hi2c, uint8_t address, uint8_t reg, uint8_t *value);
uint8_t PAC1934_Init(I2C_HandleTypeDef *hi2c);
uint8_t PAC1934_Refresh(I2C_HandleTypeDef *hi2c);
uint8_t PAC1934_RefreshV(I2C_HandleTypeDef *hi2c);
uint8_t PAC1934_GetVoltage(I2C_HandleTypeDef *hi2c, uint8_t channel, float *volts);
uint8_t PAC1934_GetCurrent(I2C_HandleTypeDef *hi2c, uint8_t channel, float *amps);
uint8_t PAC1934_GetUI(I2C_HandleTypeDef *hi2c, uint8_t channel, uint16_t *volts, uint16_t *amps);
uint8_t PAC1934_GetUI_AVG(I2C_HandleTypeDef *hi2c, uint8_t channel, uint16_t *volts, uint16_t *amps);

