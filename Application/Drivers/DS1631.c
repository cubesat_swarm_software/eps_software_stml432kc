/*
 * DS1631.c
 *
 *  Created on: 12 окт. 2020 г.
 *      Author: Данил
 */

#include "DS1631.h"
static I2C_HandleTypeDef  *I2C_DS1631;
uint8_t DS1631_WriteRegister (uint8_t address, uint8_t reg, uint8_t value)
{
	if (HAL_I2C_Mem_Write(I2C_DS1631, address << 1, reg, 1, &value, 1, 1000)==HAL_OK)
		return 0;
	return 1;
}

uint8_t DS1631_ReadByte(uint8_t address, uint8_t reg, uint8_t *value)
{
	uint8_t i2c_state;
	uint8_t buf = reg;
	while((i2c_state = HAL_I2C_Master_Transmit(I2C_DS1631, address << 1, &buf, 1, 1000)) == HAL_BUSY);
	if (i2c_state == HAL_ERROR)
		return 1;
	//HAL_Delay(5);
	while((i2c_state = HAL_I2C_Master_Receive(I2C_DS1631, address << 1, &buf, 1, 1000)) == HAL_BUSY);
	if (i2c_state == HAL_ERROR)
		return 1;

	*value = buf;
	return 0;
}

uint8_t DS1631_ReadRegister(uint8_t address, uint8_t reg, uint16_t *value)
{
	uint8_t i2c_state;
	uint8_t buf[2] = {reg, 0};
	while((i2c_state = HAL_I2C_Master_Transmit(I2C_DS1631, address << 1, buf, 1, 1000)) == HAL_BUSY);
	if (i2c_state == HAL_ERROR)
		return 1;
	//HAL_Delay(5);
	while((i2c_state = HAL_I2C_Master_Receive(I2C_DS1631, address << 1, buf, 2, 1000)) == HAL_BUSY);
	if (i2c_state == HAL_ERROR)
		return 1;

	*value = (buf[0] << 8) + buf[1];
	return 0;
}

uint8_t DS1631_Init(uint8_t address, I2C_HandleTypeDef  * hi2c)
{
	uint8_t data = 0;

	I2C_DS1631 = hi2c;
	if (DS1631_WriteRegister (address, DS1631_ACCESS_CONF, data))
		return 1;
	return 0;
}

uint8_t DS1631_GetTemperature(uint8_t address, int16_t *value)
{
	int16_t buffer;

	if (DS1631_ReadRegister(address, DS1631_READ_TEMP, &buffer))
		return 1;
	*value = (int16_t) ((buffer >> 4) >> 4);
	return 0;
}

uint8_t DS1631_SetTH(uint8_t address, int16_t value)
{
	uint8_t buffer[2] = {(uint8_t)((value >> 8) & 0xFF), // Upper 8-bits
							 (uint8_t)(value & 0xFF) };     // Lower 8-bits

	if (HAL_I2C_Mem_Write(I2C_DS1631, address << 1, DS1631_ACCESS_TH, 1, buffer, 2, 1000)==HAL_OK)
			return 0;
	return 1;
}

uint8_t DS1631_SetTL(uint8_t address, int16_t value)
{
	uint8_t buffer[2] = {(uint8_t)((value >> 8) & 0xFF), // Upper 8-bits
							 (uint8_t)(value & 0xFF) };     // Lower 8-bits

	if (HAL_I2C_Mem_Write(I2C_DS1631, address << 1, DS1631_ACCESS_TL, 1, buffer, 2, 1000)==HAL_OK)
			return 0;
	return 1;
}
