#include "stm32l4xx_hal.h"
#define FIRST_PAGE_ADDRESS    0x08000000
//#define FLASH_PAGE_SIZE             0x800  //2 kbytes
#define FLASH_DW_IN_PAGE            256
uint32_t flash_erase(uint32_t address_start,uint16_t page_num);
uint32_t flash_save(void* data, uint32_t address, uint16_t data_length);
void     flash_read(uint8_t* data,uint32_t address, uint8_t n_words);
uint32_t flash_program_page(const uint8_t *data,const uint16_t page,const uint16_t pack_size);
