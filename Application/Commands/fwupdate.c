/*
 * firmware_updates.c
 *
 *  Created on: 4 апр. 2021 г.
 *      Author: Ilia
 */
#include <fwupdate.h>
#include "settings.h"
#include "log_utils.h"
#include "../Drivers/flash.h"
#include "unican.h"
#include "crc.h"
#include "array_utils.h"
#include "cmd_msg_id.h"
#include "default_settings.h"

const char *tag = "fwupdate";
static struct fwupdate_state fw_settings = {FW_IDLE,0x1,0x43,128};

static void fwupdate_update_status(void);

uint16_t fwupdate_erase(uint8_t sender, uint8_t* data){
	fw_settings.address  = array_pop32(data);
	uint16_t res=100;
	if(fw_settings.address==FLASH_SECOND_APP_ADRESS){
		uint32_t page_num = FLASH_APP_SIZE/FLASH_PAGE_SIZE;
		uint8_t res = flash_erase(FLASH_SECOND_APP_ADRESS, page_num);
		if(res==0){
			fw_settings.state = FW_WRITE_READY;
			fw_settings.fw_address_current = 0; //must not be equal to FLASH_SECOND_APP_ADRESS
			fw_settings.fw_size_current = 0;
			fw_settings.fw_package_current = 0;
		}
	}
	return 0;
}

uint16_t fwupdate_program(uint8_t sender, uint8_t* data){
	if(fw_settings.state == FW_WRITE_READY){
		uint32_t address  = array_pop32(data);
		uint16_t size = array_pop16(data+4);
		uint16_t res_write;
	    if(address>fw_settings.fw_address_current){
	    	res_write = flash_save((void*)(data+6),address,size);
	    	if(res_write==0)
	    		fw_settings.fw_size_current+=size;
	    	    fw_settings.fw_package_current+=1;
	    }
	    else{
	    	LOGI(tag,"PACKAGE POVTOR");
	    }
	    fw_settings.fw_address_current = address;
		uint8_t TXdata[6];
		TXdata[0] = (uint8_t)(address >> 0);
		TXdata[1] = (uint8_t)(address >> 8);
		TXdata[2] = (uint8_t)(address >> 16);
		TXdata[3] = (uint8_t)(address >> 24);
		TXdata[4] = (uint8_t)(res_write >> 0);
		TXdata[5] = (uint8_t)(res_write >> 8);
		struct unican_message msg_resp;
		msg_resp.data = TXdata;
		msg_resp.unican_msg_id = RESP_FWUPDATE_MEMSET_REPORT;
		msg_resp.unican_length = 6;
		msg_resp.unican_address_to = sender;
		unican_send_msg(&msg_resp);
		fwupdate_update_status();
		return res_write;
	}
	else{
		LOGI(tag,"YOU NEED ERASE MEMORY BEFORE!");
		return 100;
	}
}

struct fwupdate_state * fwupdate_get(){
	return &fw_settings;
}

static void fwupdate_update_status(void){
	if(fw_settings.fw_size_current>=FLASH_APP_SIZE){
		uint32_t calculatedCrc = HAL_CRC_Accumulate(&hcrc, (uint32_t *)FLASH_SECOND_APP_ADRESS, BUFFER_CRC_SIZE_WORDS);
		fw_settings.state = (calculatedCrc == *(uint32_t*)FLASH_CRC_SECOND_APP)? FW_UPDATE_DONE: FW_UPDATE_ERROR;
	}
}

void fwupdate_set_bootloader_flag(){
	uint32_t calculatedCrc = HAL_CRC_Accumulate(&hcrc, (uint32_t *)FLASH_SECOND_APP_ADRESS, BUFFER_CRC_SIZE_WORDS);
	uint16_t res;
	if(calculatedCrc == *(uint32_t*)FLASH_CRC_SECOND_APP){
		res = flash_erase(FLASH_BOOTLOADER_FLAG, 1);
		if(res!=0){
			return 100;
		}
		res = flash_save(1, FLASH_BOOTLOADER_FLAG, 1);
		if(res!=0)
		{
			return 101;
		}
	}
	else{
		return 102;
	}
	return 0;
}



