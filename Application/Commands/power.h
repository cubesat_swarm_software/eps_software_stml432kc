#include "main.h"

typedef enum {CH1=1, CH2=2, CH3=3, CH4=4, CH_RFM=5} channels;
typedef enum {OFF=0, ON=1} channel_states;

int power_sw_on(uint8_t channel);

int power_sw_off(uint8_t channel);

int power_sw_reset(uint8_t channel);
