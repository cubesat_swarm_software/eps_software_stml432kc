/*
 * common_cmd.h
 *
 *  Created on: 9 апр. 2021 г.
 *      Author: Ilia
 */

#ifndef COMMANDS_COMMON_CMD_H_
#define COMMANDS_COMMON_CMD_H_

#include <stdint.h>
#include <stdbool.h>
#include "stm32l4xx_hal.h"

int get_firmare_version(uint8_t sender, uint8_t *data);

int get_logbook_size(uint8_t sender, uint8_t *data);

int get_logbook_record(uint8_t sender, uint8_t *data);

int logbook_erase(uint8_t sender, uint8_t *data);

int get_fwupdate_state(uint8_t sender, uint8_t *data);

int get_onboard_time(uint8_t sender, uint8_t *data);

int set_onboard_time(uint8_t sender, uint8_t *data);

int give_status(uint8_t sender, uint8_t *data);

int reset(uint8_t sender, uint8_t *data);



#endif /* COMMANDS_COMMON_CMD_H_ */
