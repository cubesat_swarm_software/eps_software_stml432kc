
//#define cmd_send(cmd) if(cmd != NULL){xQueueSend(dispatcher_queue, &cmd, portMAX_DELAY);}

#include <stdint.h>
#include <stdbool.h>
#include "stm32l4xx_hal.h"
typedef int (*cmdfunction)(uint8_t sender,uint8_t *args);

typedef enum {
    CMD_OK = 0x00,
    CMD_WRONG_PARAM = 0x01,
    CMD_NOT_FOUND = 0x02,
} CMD_StatusTypeDef;


typedef struct cmd_d {
    uint16_t     msg_id;
    cmdfunction  function;       ///< Command function
    uint8_t      fix_cmd_size;   //1 if cmd must get curtan amount of bytes as argument  0 if it is not fix
    uint8_t      n_bytes;
} cmd_descr;


struct cmd_t {
    uint8_t *args;
    uint8_t id_cmd;
    uint8_t sender;
};



void cmd_repo_init();

uint16_t cmd_check(uint16_t msg_id, uint8_t n_bytes, uint8_t *cmd_table_id);
uint16_t cmd_run(uint8_t id, uint8_t sender, uint8_t *args);

