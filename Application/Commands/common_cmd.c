#include "common_cmd.h"
#include "unican.h"
#include "log_utils.h"
#include "settings.h"
#include "logbook.h"
#include "fwupdate.h"
#include "cmd_msg_id.h"
#include "time.h"
#include "status.h"
#include "cmd_msg_id.h"
static const char *tag = "common_cmd";

int get_firmare_version(uint8_t sender, uint8_t *data) {
    uint8_t data_vers;
    struct settings *dev = settings_get();
    data_vers = (uint8_t) dev->fw_version;
    struct unican_message msg;
    msg.data = &data_vers;
    msg.unican_address_to = sender;
    msg.unican_length = 2;
    msg.unican_msg_id = RESP_VERSION_SW;
    unican_send_msg(&msg);
    return 0;
}

int get_logbook_size(uint8_t sender, uint8_t *data){
	uint16_t res = logbook_get_size();
    unican_send_u16(sender, res, RESP_LOGBOOK_SIZE);
    return 0;
}

int get_logbook_record(uint8_t sender, uint8_t *data){
    struct Logbook_t logbook;
	uint16_t res = logbook_read(&logbook,data[0]);
	if(res==0){
		struct unican_message msg;
		msg.data = (uint8_t*)(&logbook);
		msg.unican_address_to = sender;
		msg.unican_length = 16;
		msg.unican_msg_id = RESP_LOGBOOK_RECORD;
		unican_send_msg(&msg);
	}
    return res;
}

int get_fwupdate_state(uint8_t sender, uint8_t *data){
	struct fwupdate_state * fw_up_state = fwupdate_get();
    struct unican_message msg;
    msg.data = (uint8_t*)fw_up_state;
    msg.unican_address_to = sender;
    msg.unican_length = 13;
    msg.unican_msg_id = RESP_FWUPDATE_STATE;
    unican_send_msg(&msg);
    return 0;
}

int get_onboard_time(uint8_t sender, uint8_t *data){
	uint16_t res = logbook_get_size();
	struct time_board time;
	struct unican_message msg;
	time_get(&time);
    msg.data = (uint8_t*)&time;
    msg.unican_address_to = sender;
    msg.unican_length = 4;
    msg.unican_msg_id = RESP_ONBOARD_TIME;
    unican_send_msg(&msg);
    return 0;
}

int logbook_erase(uint8_t sender, uint8_t *data){
	uint16_t res = logbook_free();
	return res;
}

int reset(uint8_t sender, uint8_t *data){
	NVIC_SystemReset();
	return 0;
}

int give_status(uint8_t sender, uint8_t *data) {
	struct status *dev_status = status_get();
	struct unican_message msg;
	msg.data = (uint8_t*)(dev_status);
	msg.unican_address_to = sender;
	msg.unican_length = 7;
	msg.unican_msg_id = RESP_GET_STATUS;
	unican_send_msg(&msg);
	return 0;
}

int set_onboard_time(uint8_t sender, uint8_t *data){
	RTC_TimeTypeDef sTime;
	RTC_DateTypeDef sDate;
	struct time_board *time;
	time = data;
	sDate.Date  = time->days;
	sTime.Hours = time->hours;
	sTime.Minutes = time->minutes;
	sTime.Seconds = time->seconds;
	time_set(&sTime, &sDate);
	return 0;
}
