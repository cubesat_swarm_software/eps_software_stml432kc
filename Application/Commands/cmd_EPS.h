/*
 * cmd_OBC.h
 *
 *  Created on: 17 ���. 2021 �.
 *      Author: Ilia
 */

#ifndef INC_CMD_OBC_H_
#define INC_CMD_OBC_H_
#include <stdint.h>
#include <stdbool.h>
#include "stm32l4xx_hal.h"
#include "repo_data_schema.h"
#include "main.h"

int Pwr_sw_off(uint8_t sender,uint8_t * data);
int Pwr_sw_on(uint8_t sender,uint8_t *data);
int Pwr_sw_reset(uint8_t sender,uint8_t *data);
#endif /* INC_CMD_OBC_H_ */
