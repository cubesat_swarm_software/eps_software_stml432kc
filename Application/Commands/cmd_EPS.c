/*
 * cmd_OBC.c
 *
 *  Created on: 17 ���. 2021 �.
 *      Author: Ilia
 */

#ifndef SRC_CMD_OBC_C_
#define SRC_CMD_OBC_C_
#include <cmd_EPS.h>
#include "unican.h"
#include "settings.h"
#include "board.h"
#include "status.h"
#include "power.h"
static const char * tag = "Cmd_EPC";

int Pwr_sw_on(uint8_t sender,uint8_t *data){
	uint8_t channel = data[0];
	if(sender==UNICAN_GROUND_ADDRESS){
		return power_sw_on(channel);
	}
	return 1;
}

int Pwr_sw_off(uint8_t sender,uint8_t *data){
	uint8_t channel = data[0];
	if(sender==UNICAN_GROUND_ADDRESS){
		return power_sw_off(channel);
	}
	return 1;
}

int Pwr_sw_reset(uint8_t sender,uint8_t *data){
	uint8_t channel = data[0];
	if(sender==UNICAN_GROUND_ADDRESS){
		return power_sw_reset(channel);
	}
	return 1;
}
#endif /* SRC_CMD_OBC_C_ */
