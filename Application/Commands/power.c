/*
 * power.c
 *
 *  Created on: 15 апр. 2021 г.
 *      Author: Ilia
 */
#include "power.h"
#include "status.h"
#include "settings.h"
#include "../../Drivers/rfm69/rfm69.h"
#include "../../Application/Drivers/RFM69_ex.h"

int power_sw_on(uint8_t channel){
	struct status * dev_status = status_get();
	switch (channel){
	   case CH1:
		   if(dev_status->ch1_enable){
			   OBC_ON
			   dev_status->ch1 = 1;
		   }
		   else{
			   return 1;
		   }
		   break;
	   case CH2:
		   if(dev_status->ch2_enable){
			   UHF_ON
			   dev_status->ch2 = 1;
		   }
		   else{
			   return 1;
		   }
		   break;
	   case CH3:

		   break;
	   case CH4:

		   //dev_status->ch4=1;
		   break;
	   case CH_RFM:
		   if(dev_status->ch_rfm_enable){
			   RFM69_ON
				setDefaultCfg();
				//HAL_Delay(20);//Delay for power supply rfm69
				osDelay(20);
				rfm69_LibInit(
							 &UM_SPI1_transfer,
							 &rfm69_CS_set_LOW,
							 &rfm69_CS_set_HIGH,
							 0,
							 0,
							 0,
							 0,
							 0,
							 0,
							 0,
							 &um_delay_us,
							 cfg_current.fields.rf_packet_length,
							 0,
							 0,
							 &rfm69_get_tick_us
						   );
			   updateRfm69Cfg();
			   dev_status->ch_rfm =1;
		   }
		   else{
			   return 1;
		   }
		   //dev_status->ch4=1;
		   break;
	   default:
		   break;
	}
	return 0;
}

int power_sw_off(uint8_t channel){
	struct status * dev_status = status_get();
	switch (channel){
	   case CH1:
		   if(dev_status->ch1_enable){
			   OBC_OFF
			   dev_status->ch1 = 0;
		   }
		   else{
			   return 1;
		   }
		   break;
	   case CH2:
		   if(dev_status->ch2_enable){
			   UHF_OFF//очень плохая команда,c земли наверно лучше такого не посылать (лучше убрать
			   dev_status->ch2 = 0;
		   }
		   else{
			   return 1;
		   }
		   break;
	   case CH3:
		   //dev_status->ch4 = 0;
		   break;
	   case CH4:
		   //dev_status->ch4 = 0;
		   break;
	   case CH_RFM:
		   if(dev_status->ch_rfm_enable){
			   RFM69_OFF
			   dev_status->ch_rfm = 0;
		   }
		   else{
			   return 1;
		   }
		   break;
	   default:
		   break;
	}
	return 0;
}


int power_sw_reset(uint8_t channel){
	struct status * dev_status = status_get();
	struct settings * dev_settings = settings_get();
	switch (channel){
	   case CH1:
		   if(dev_status->ch1_reset_n <= dev_settings->PCH1_reset_n){
			   OBC_OFF
			   osDelay(dev_settings->PCH1_reset_delay);
			   //HAL_Delay(dev_settings->PCH1_reset_delay);
			   OBC_ON
		   }
		   else{
			   return 1;
		   }
		   break;
	   case CH2:
		   if(dev_status->ch2_reset_n<=dev_settings->PCH2_reset_n){
			   UHF_OFF
			   //HAL_Delay(dev_settings->PCH2_reset_delay);
			   osDelay(dev_settings->PCH2_reset_delay);
			   UHF_ON
		   }
		   else{
			   return 1;
		   }
		   break;
	   case CH3:
		   if(dev_status->ch2_reset_n<=dev_settings->PCH2_reset_n){
			   RFM69_OFF
			   osDelay(dev_settings->PCH3_reset_delay);
			   //HAL_Delay(dev_settings->PCH3_reset_delay);
			   RFM69_ON
		   }
		   else{
			   return 1;
		   }
		   break;
	   default:
		   break;
	}
	return 0;
}
