/*******************************************************************************
*                                                                              *
*      Данный файл содержит адреса регистров приёмопередатчика RFM69HW         *
*                                                                              *
*******************************************************************************/

/*-----------------------------Адреса регистров------------------------------*/
#define RH_RF69_REG_00_FIFO                                 0x00              // FIFO read/write access
#define RH_RF69_REG_01_OPMODE                               0x01              // Operating modes of the transceiver
#define RH_RF69_REG_02_DATAMODUL                            0x02              // Data operation mode and Modulation settings
#define RH_RF69_REG_03_BITRATEMSB                           0x03              // Bit Rate setting, Most Significant Bits
#define RH_RF69_REG_04_BITRATELSB                           0x04              // Bit Rate setting, Least Significant Bits
#define RH_RF69_REG_05_FDEVMSB                              0x05              // Frequency Deviation setting, Most Significant Bits
#define RH_RF69_REG_06_FDEVLSB                              0x06              // Frequency Deviation setting, Least Significant Bits
#define RH_RF69_REG_07_FRFMSB                               0x07              // RF Carrier Frequency, Most Significant Bits
#define RH_RF69_REG_08_FRFMID                               0x08              // RF Carrier Frequency, Intermediate Bits
#define RH_RF69_REG_09_FRFLSB                               0x09              // RF Carrier Frequency, Least Significant Bits
#define RH_RF69_REG_0A_OSC1                                 0x0a              // RC Oscillators Settings
#define RH_RF69_REG_0B_AFCCTRL                              0x0b              // AFC control in low modulation index situations
#define RH_RF69_REG_0C_RESERVED                             0x0c              // -
#define RH_RF69_REG_0D_LISTEN1                              0x0d              // Listen Mode settings
#define RH_RF69_REG_0E_LISTEN2                              0x0e              // Listen Mode Idle duration
#define RH_RF69_REG_0F_LISTEN3                              0x0f              // Listen Mode Rx duration
#define RH_RF69_REG_10_VERSION                              0x10              // -
#define RH_RF69_REG_11_PALEVEL                              0x11              // PA selection and Output Power control
#define RH_RF69_REG_12_PARAMP                               0x12              // Control of the PA ramp time in FSK mode
#define RH_RF69_REG_13_OCP                                  0x13              // Over Current Protection control
#define RH_RF69_REG_14_RESERVED                             0x14              // -
#define RH_RF69_REG_15_RESERVED                             0x15              // -
#define RH_RF69_REG_16_RESERVED                             0x16              // -
#define RH_RF69_REG_17_RESERVED                             0x17              // -
#define RH_RF69_REG_18_LNA                                  0x18              // LNA settings
#define RH_RF69_REG_19_RXBW                                 0x19              // Channel Filter BW Control
#define RH_RF69_REG_1A_AFCBW                                0x1a              // Channel Filter BW control during the AFC routine
#define RH_RF69_REG_1B_OOKPEAK                              0x1b              // OOK demodulator selection and control in peak mode
#define RH_RF69_REG_1C_OOKAVG                               0x1c              // Average threshold control of the OOK demodulator
#define RH_RF69_REG_1D_OOKFIX                               0x1d              // Fixed threshold control of the OOK demodulator
#define RH_RF69_REG_1E_AFCFEI                               0x1e              // AFC and FEI control and status
#define RH_RF69_REG_1F_AFCMSB                               0x1f              // MSB of the frequency correction of the AFC
#define RH_RF69_REG_20_AFCLSB                               0x20              // LSB of the frequency correction of the AFC
#define RH_RF69_REG_21_FEIMSB                               0x21              // MSB of the calculated frequency error
#define RH_RF69_REG_22_FEILSB                               0x22              // LSB of the calculated frequency error
#define RH_RF69_REG_23_RSSICONFIG                           0x23              // RSSI-related settings
#define RH_RF69_REG_24_RSSIVALUE                            0x24              // RSSI value in dBm
#define RH_RF69_REG_25_DIOMAPPING1                          0x25              // Mapping of pins DIO0 to DIO3
#define RH_RF69_REG_26_DIOMAPPING2                          0x26              // Mapping of pins DIO4 and DIO5, ClkOut frequency
#define RH_RF69_REG_27_IRQFLAGS1                            0x27              // Status register: PLL Lock state, Timeout, RSSI > Threshold...
#define RH_RF69_REG_28_IRQFLAGS2                            0x28              // Status register: FIFO handling flags...
#define RH_RF69_REG_29_RSSITHRESH                           0x29              // RSSI Threshold control
#define RH_RF69_REG_2A_RXTIMEOUT1                           0x2a              // Timeout duration between Rx request and RSSI detection
#define RH_RF69_REG_2B_RXTIMEOUT2                           0x2b              // Timeout duration between RSSI detection and PayloadReady
#define RH_RF69_REG_2C_PREAMBLEMSB                          0x2c              // Preamble length, MSB
#define RH_RF69_REG_2D_PREAMBLELSB                          0x2d              // Preamble length, LSB
#define RH_RF69_REG_2E_SYNCCONFIG                           0x2e              // Sync Word Recognition control
#define RH_RF69_REG_2F_SYNCVALUE1                           0x2f              // Sync Word bytes, 1
#define RH_RF69_REG_30_SYNCVALUE2                           0x30              // Sync Word bytes, 2
#define RH_RF69_REG_31_SYNCVALUE3                           0x31              // Sync Word bytes, 3
#define RH_RF69_REG_32_SYNCVALUE4                           0x32              // Sync Word bytes, 4
#define RH_RF69_REG_33_SYNCVALUE5                           0x33              // Sync Word bytes, 5
#define RH_RF69_REG_34_SYNCVALUE6                           0x34              // Sync Word bytes, 6
#define RH_RF69_REG_35_SYNCVALUE7                           0x35              // Sync Word bytes, 7
#define RH_RF69_REG_36_SYNCVALUE8                           0x36              // Sync Word bytes, 8
#define RH_RF69_REG_37_PACKETCONFIG1                        0x37              // Packet mode settings
#define RH_RF69_REG_38_PAYLOADLENGTH                        0x38              // Payload length setting
#define RH_RF69_REG_39_NODEADRS                             0x39              // Node address
#define RH_RF69_REG_3A_BROADCASTADRS                        0x3a              // Broadcast address
#define RH_RF69_REG_3B_AUTOMODES                            0x3b              // Auto modes settings
#define RH_RF69_REG_3C_FIFOTHRESH                           0x3c              // Fifo threshold, Tx start condition
#define RH_RF69_REG_3D_PACKETCONFIG2                        0x3d              // Packet mode settings
#define RH_RF69_REG_3E_AESKEY1                              0x3e              // 1_st byte of the cypher key
#define RH_RF69_REG_3F_AESKEY2                              0x3f              // 2_nd byte of the cypher key
#define RH_RF69_REG_40_AESKEY3                              0x40              // 3_rd byte of the cypher key
#define RH_RF69_REG_41_AESKEY4                              0x41              // 4_th byte of the cypher key
#define RH_RF69_REG_42_AESKEY5                              0x42              // 5_th byte of the cypher key
#define RH_RF69_REG_43_AESKEY6                              0x43              // 6_th byte of the cypher key
#define RH_RF69_REG_44_AESKEY7                              0x44              // 7_th byte of the cypher key
#define RH_RF69_REG_45_AESKEY8                              0x45              // 8_th byte of the cypher key
#define RH_RF69_REG_46_AESKEY9                              0x46              // 9_th byte of the cypher key
#define RH_RF69_REG_47_AESKEY10                             0x47              // 10_th byte of the cypher key
#define RH_RF69_REG_48_AESKEY11                             0x48              // 11_th byte of the cypher key
#define RH_RF69_REG_49_AESKEY12                             0x49              // 12_th byte of the cypher key
#define RH_RF69_REG_4A_AESKEY13                             0x4a              // 13_th byte of the cypher key
#define RH_RF69_REG_4B_AESKEY14                             0x4b              // 14_th byte of the cypher key
#define RH_RF69_REG_4C_AESKEY15                             0x4c              // 15_th byte of the cypher key
#define RH_RF69_REG_4D_AESKEY16                             0x4d              // 16_th byte of the cypher key
#define RH_RF69_REG_4E_TEMP1                                0x4e              // Temperature Sensor control
#define RH_RF69_REG_4F_TEMP2                                0x4f              // Temperature readout
#define RH_RF69_REG_58_TESTLNA                              0x58              // Sensitivity boost
#define RH_RF69_REG_5A_TESTPA1                              0x5a              // High Power PA settings
#define RH_RF69_REG_5C_TESTPA2                              0x5c              // High Power PA settings
#define RH_RF69_REG_6F_TESTDAGC                             0x6f              // Fading Margin Improvement
#define RH_RF69_REG_71_TESTAFC                              0x71              // AFC offset for low modulation index AFC
/*-------------------------Конец адресов регистров---------------------------*/
