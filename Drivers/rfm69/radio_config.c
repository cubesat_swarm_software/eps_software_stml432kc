#include "radio_config.h"
#include "rfm69.h"
volatile yarilo_radio_config cfg_current;//структура, хранящая текущие настройки трансивера

#define CFG_CORRECTNESS_KEY (0x1D171311) //ключ который должен быть записан в поле корректности, чтобы данные считались корректными

void setDefaultCfg()
{
  cfg_current.fields.freqRX = 433.0;
  cfg_current.fields.freqTX = 433.0;
  cfg_current.fields.baudRateTx_id = 3;//id скорости передачи данных
  cfg_current.fields.baudRateRx_id = 3;//id скорости приёма данных
  cfg_current.fields.fdevRx = 9600.0;//Частота сдвига при приёме
  cfg_current.fields.fdevTx = 9600.0;//Частота сдвига при передаче
	cfg_current.fields.rxFilter_id = (uint32_t)rfm69_RxBw_25_0;//id фильтра на приём
  cfg_current.fields.rxFilterDcc_id = 6;//настройка DC-canceller, нужно экспериментально определять, какая лучше
  cfg_current.fields.modulation_tx = 0;//режим модуляции при передаче - сейчас не реализовано на уровне драйвера RFM69
  cfg_current.fields.modulation_rx = 0;//режим модуляции при приёме - сейчас не реализовано на уровне драйвера RFM69
  cfg_current.fields.modulation_filter_rx = 0;//режим фильтра модуляции при приёме
  cfg_current.fields.modulation_filter_tx = 0;//режим фильтра модуляции при приёме
  cfg_current.fields.dac_to_output_power_dBm_coef[0] = 2977.69;//коэффициенты зависимости значения ЦАП от выходной мощности
  cfg_current.fields.dac_to_output_power_dBm_coef[1] = 203.363;
	cfg_current.fields.dac_to_output_power_dBm_coef[2] = -106.363;
	cfg_current.fields.dac_to_output_power_dBm_coef[3] = 24.6947;
	cfg_current.fields.txPower_W = 0.6;//выходная мощность в Вт - штатное значение по-умолчанию
  cfg_current.fields.rfm69_output_power_dBm = 10;//выходная мощность микросборки RFM69HW
  cfg_current.fields.txPower_DAC = 3160;//текущее значение выхода DAC, Pout = 1 Вт
  cfg_current.fields.isBypass_power_amplifier_for_TX = 0;//исключать ли усилитель мощности из тракта передачи
  cfg_current.fields.rf_packet_length = 24;//длина единичного радиопакета
  cfg_current.fields.rf_preambule_length = 3;//длина преамбулы
  cfg_current.fields.rf_sync_length = 5;//длина синхрослова
#ifdef SATELLITE_NUMBER_1
  uint8_t callsign[] = {'R','S','2','1','S'}; 
#elif SATELLITE_NUMBER_2
  uint8_t callsign[] = {'R','S','2','2','S'};
#else
  uint8_t callsign[] = {'R','S','2','x','S'};
#endif
  cfg_current.fields.sync_word_low = (((uint32_t)callsign[0])) | (((uint32_t)callsign[1]) << 8) | (((uint32_t)callsign[2]) << 16) | (((uint32_t)callsign[3]) << 24);//первые 4 байта синхрослова
  cfg_current.fields.sync_word_high = callsign[4];//вторые 4 байта синхрослова 
	cfg_current.fields.rf_allowable_bit_errors = 2;//допустимое число битовых ошибок в синхрослове
  cfg_current.fields.max_bus_frames_in_rf_packet = 5;//максимальное число кадров шины в радиопакете
  cfg_current.fields.isUseCodding = 1;//флаг использовать ли кодирование
  cfg_current.fields.bytesForCodding_NPAR = 4;//4 байта для кодирования используем по-умолчанию
  cfg_current.fields.mcu_idle_freq_id = 0;//id частоты МК в режим простоя
  cfg_current.fields.mcu_rxtx_freq_id = 0;//id частоты МК в режиме приёма/передачи
  cfg_current.fields.wait_frames_for_send_delay = 0.020;//период ожидания дополнительных пакетов для отправки
  cfg_current.fields.after_send_delay = 0.0001;//задержка после отправки пакета
  cfg_current.fields.radio_task_delay = 0.0001;//задержка в конце задачи радио
  cfg_current.fields.process_task_delay = 0.0001;//задержка в конце задачи обработки приёма
  cfg_current.fields.tmi_update_period = 5.0;//период обновления внуренней телеметрии
  cfg_current.fields.monitor_period = 1.0;//период работы задачи-монитора
  cfg_current.fields.fram_tmi_write_position = 0;//положение лога тми в FRAM
  cfg_current.fields.time_save_period_s = 11;
  cfg_current.fields.tmi_save_to_fram_period_s = 200;
  cfg_current.fields.tmi_send_period_s = 30;//штатное значение - маяк
  cfg_current.fields.config_correct_status = CFG_CORRECTNESS_KEY;
}
